// Implementation of the function getmem
// Authors: Jason Ho, Thomas Johnson

#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"
#include "mem_implEC.h"

Block* init_block(uintptr_t size);
Block* insert_block(Block *block);

// initialize a free list block of specified size (expects that the user's
// specified size included space for the header) return a pointer to the
// front of the block
Block* init_block(uintptr_t size) {
  // assure that malloc is taking in a multiple of 16
  assert(size % 16 == 0);
  assert(size >= MIN_BLOCK_SIZE);
  Block* new_block = malloc(size + HEADER_SIZE);
  // make sure malloc suceeded
  if (new_block == NULL) {
    fprintf(stderr, "init_block: malloc failed\n");
    exit(1);
  }

  new_block->size = size;
  new_block->next = NULL;
  new_block->prev = NULL;
  new_block->free_flag = 1;

  insert_block(new_block);
  
  return new_block;
}

Block* insert_block(Block *block) {
  if (free_list == NULL) {
    free_list = block;
  } else if ((uintptr_t)block < (uintptr_t)free_list) {
    // case where block becomes head
    block->next = free_list;
    free_list->prev = block;
    free_list = block;
  } else {
    Block *free_list_iterator = free_list;
    while (free_list_iterator->next != NULL) {
      if ((uintptr_t)block < (uintptr_t)free_list_iterator->next) {
        block->next = free_list_iterator->next;
        block->prev = free_list_iterator;
        block->next->prev = block;
	free_list_iterator->next = block;
	break;
      }

      free_list_iterator = free_list_iterator->next;
    }

    // catch case where block is tail
    if (free_list_iterator->next == NULL) {
      free_list_iterator->next = block;
      block->prev = free_list_iterator;
    }
  }

  // the ammount we grab with malloc
  g_total_size += block->size + HEADER_SIZE;
  // add to total free space the size of the available bytes
  g_total_free += block->size + HEADER_SIZE;
  // we create 1 new block
  g_free_blocks++;

  return block;
}

// Input "size" is an unsigned integer and must be greater than
// 0. If size <= 0 then return value is NULL. Otherwise returns
// a pointer to a new block of storage with at least "size" bytes
// of memory.
void* getmem(uintptr_t size) {
  check_heap();

  if (size == 0) {
    return NULL;
  }

  // requested size rounded up to nearest multiple of 16
  size = size + (16 - (size % 16));

  // in case user requested a small amount of bytes (<= 16), make sure
  // the user gets at least the minimum block size of bytes back
  if (size < MIN_BLOCK_SIZE) {
    size = MIN_BLOCK_SIZE;
  }

  if (search_head == NULL) {
    search_head = free_list;
  }

  uintptr_t init_size;
  if (size > STANDARD_BLOCK_SIZE) {
    init_size = size;
  } else {
    init_size = STANDARD_BLOCK_SIZE;
  }
  
  Block* return_block = NULL;

  if (free_list == NULL) {
    // if we have not requested any storage from the underlying system yet
    return_block = init_block(init_size);
  } else if (search_head->free_flag == 1 && search_head->size >= size){
    // this block is good enough and is free
    return_block = search_head;
  } else {
    // we have to do an actual search
    Block *free_list_iterator = search_head->next;
    if (free_list_iterator == NULL) {
      free_list_iterator = free_list;
    }

    // iterate over list until we get back to the start
    while ((uintptr_t)free_list_iterator != (uintptr_t)search_head) {
      // we found a block that's big enough and free
      if (free_list_iterator-> free_flag == 1 && free_list_iterator->size >= size) {
	return_block = free_list_iterator;
	break;
      }

      // wrap around if necessary
      free_list_iterator = free_list_iterator->next;
      if (free_list_iterator == NULL) {
	free_list_iterator = free_list;
      }
    }

    // we couldn't find a block, make a new one
    if (return_block == NULL) {
      return_block = init_block(init_size);
    }
  }

  // check if we can split or not
  if (return_block->size - MIN_BLOCK_SIZE >= size + HEADER_SIZE) {
    // we can split
    return_block->size -= size + HEADER_SIZE;
    Block *new_block = (Block*)((uintptr_t)return_block + return_block->size + HEADER_SIZE);
    new_block->size = size;
    new_block->free_flag = 0;
    new_block->next = return_block->next;
    new_block->prev = return_block;
    if (new_block->next != NULL) {
      new_block->next->prev = new_block;
    }
    new_block->prev->next = new_block;
    
    g_free_blocks++;
    
    return_block = new_block;
  } else {
    // we cannot split
    Block *free_list_iterator = search_head->next;
    if (free_list_iterator == NULL) {
      free_list_iterator = free_list;
    }

    // move search head to the next free block before returning this one
    while ((uintptr_t)free_list_iterator != (uintptr_t)search_head) {
      if (free_list_iterator->free_flag == 1) {
	search_head = free_list_iterator;
	break;
      }
      
      free_list_iterator = free_list_iterator->next;
      if (free_list_iterator == NULL) {
	free_list_iterator = free_list;
      }
    }

    // there are no free blocks, set search head to the front of the list
    if ((uintptr_t)free_list_iterator == (uintptr_t)search_head) {
      search_head = free_list;
    }

    return_block->free_flag = 0;
  }

  g_free_blocks--;
  g_total_free -= return_block->size + HEADER_SIZE;
  
  // get the void pointer
  void *vp = (void*)((uintptr_t)return_block + HEADER_SIZE); 
  
  check_heap();
  return vp;
}
