// Implementation of the function check_heap
// Authors: Jason Ho, Thomas Johnson

#include "mem_impl.h"
#include "mem.h"
#include <assert.h>
#include <inttypes.h>

// Check for possible problems with the free list data structure.
// Makes the following checks:
// - blocks are ordered with strictly increasing memory addresses
// - block sizes are positive numbers and not smaller than the
//   chosen minimum block size
// - blocks do not overlap
// - blocks are not touching
// If no errors are detected than function returns silently.
void check_heap() {
  Block* free_list_iterator = free_list;
  if (free_list_iterator != NULL) {
    // first block size is positive and not smaller than minimum block size
    assert(free_list_iterator->size >= MIN_BLOCK_SIZE);

    while (free_list_iterator->next != NULL) {
      // the rest of the block sizes are positive and not smaller than the
      // minimum block size
      assert(free_list_iterator->next->size >= MIN_BLOCK_SIZE);

      // blocks are ordered with strictly increasing memory adresses
      assert((uintptr_t)free_list_iterator <
             (uintptr_t)free_list_iterator->next);

      // blocks do not overlap (the start + length of a block is not an
      // address in the middle of a later block on the list)
      // also ensures that blocks are not touching
      assert((uintptr_t)free_list_iterator + free_list_iterator->size
             + HEADER_SIZE < (uintptr_t)free_list_iterator->next);

      free_list_iterator = free_list_iterator->next;
    }
  }
}
