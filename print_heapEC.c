// Implementation of the function print_heap
// Authors: Jason Ho, Thomas Johnson

#include "mem.h"
#include "mem_implEC.h"

// Print a formatted listing to file f showing the blocks on the free
// list.
void print_heap(FILE* f) {
  fprintf(f, "PRINT HEAP:\n");

  Block* free_list_iterator = free_list;
  while (free_list_iterator != NULL) {
    if (free_list_iterator->free_flag == 1) {  
      fprintf(f, "%p : %lu bytes (%lu bytes with header)\n",
              free_list_iterator,
              free_list_iterator->size,
		free_list_iterator->size + HEADER_SIZE);
    }

    free_list_iterator = free_list_iterator->next;
  }

  fprintf(f, "\n");
  return;
}
