// Implementation of the function print_heap
// Authors: Jason Ho, Thomas Johnson

#include "mem_impl.h"
#include "mem.h"

// Print a formatted listing to file f showing the blocks on the free
// list.
void print_heap(FILE* f) {
  fprintf(f, "PRINT HEAP:\n");

  Block* free_list_iterator = free_list;
  while (free_list_iterator != NULL) {
    fprintf(f, "%p : %lu bytes (%lu bytes with header)\n",
            free_list_iterator,
            free_list_iterator->size,
            free_list_iterator->size + HEADER_SIZE);
    free_list_iterator = free_list_iterator->next;
  }

  fprintf(f, "\n");
  return;
}
