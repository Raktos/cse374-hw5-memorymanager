// Implementation of the function get_mem_stats
// Authors: Jason Ho, Thomas Johnson

#include "mem_impl.h"
#include "mem.h"

// Store statistics about the current state of the memory manager
// in the three integer variables whose addresses are given as
// arguments.
//
// total_size: total amount of storage in bytes acquired by the
//             memory manager so far to use in satisfying allocation
//             requests (total amount requested by underlying system)
// total_free: the total amount of storage in bytes that is currently
//             stored in the free list
// n_free_blocks: the total number of individual blocks currently
//                stored on the free list
void get_mem_stats(uintptr_t* total_size, uintptr_t* total_free,
                   uintptr_t* n_free_blocks) {
  *total_size = g_total_size;
  *total_free = g_total_free;
  *n_free_blocks = g_free_blocks;
}
