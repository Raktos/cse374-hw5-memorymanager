// Implementation of the function getmem
// Authors: Jason Ho, Thomas Johnson

#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"
#include "mem_impl.h"

Block* init_block(uintptr_t);


// initialize a free list block of specified size (expects that the user's
// specified size included space for the header) return a pointer to the
// front of the block
Block* init_block(uintptr_t size_w_head) {
  // assure that malloc is taking in a multiple of 16
  assert(size_w_head % 16 == 0);
  Block* new_block = malloc(size_w_head);
  // make sure malloc suceeded
  if (new_block == NULL) {
    fprintf(stderr, "init_block: malloc failed\n");
    exit(1);
  }

  new_block->size = size_w_head - HEADER_SIZE;
  new_block->next = NULL;

  // the ammount we grab with malloc
  g_total_size += size_w_head;
  // add to total free space the size of the available bytes
  g_total_free += size_w_head;
  // we create 1 new block
  g_free_blocks++;

  return new_block;
}

// Input "size" is an unsigned integer and must be greater than
// 0. If size <= 0 then return value is NULL. Otherwise returns
// a pointer to a new block of storage with at least "size" bytes
// of memory.
void* getmem(uintptr_t size) {
  check_heap();

  if (size == 0) {
    return NULL;
  }

  // requested size rounded up to nearest multiple of 16
  size = size + (16 - (size % 16));

  // in case user requested a small amount of bytes (<= 16), make sure
  // the user gets at least the minimum block size of bytes back
  if (size < MIN_BLOCK_SIZE) {
    size = MIN_BLOCK_SIZE;
  }

  Block* free_list_iterator = free_list;
  // if we have not requested any storage from the underlying system yet
  if (free_list_iterator == NULL) {
    // allocate 4096 + size (+ header sizes) bytes to free list, return
    // pointer to block with input size to user, keeping a block with
    // size 4096 on the free list
    //
    // grabs a large block then splits off the actual request size at
    // the end

    free_list_iterator = init_block(STANDARD_BLOCK_SIZE + size +
                                    2 * HEADER_SIZE);
    free_list_iterator->size = STANDARD_BLOCK_SIZE;

    // create new block at address where we want to split
    Block *block = (Block*)((uintptr_t)free_list_iterator + HEADER_SIZE +
                            STANDARD_BLOCK_SIZE);
    // set the size parameter for the new block
    block->size = size;

    // subtract from the total free memory the total size of the block
    // we are returning
    g_total_free -= size + HEADER_SIZE;

    free_list = free_list_iterator;

    // return void pointer to the location of the end of the header of
    // the new block
    void *vp = (void *)((uintptr_t)block + HEADER_SIZE);
    check_heap();
    return vp;
  }

  // iterate over existing free list.
  // treat the front of the list specially
  if (free_list_iterator->size >= size) {
    // if the first block has enough size to split it first
    if (free_list_iterator->size - MIN_BLOCK_SIZE >= HEADER_SIZE + size) {
      // decriment the size of the block that is too large by the new size
      // requested including room for the header of the new block
      free_list_iterator->size -= size + HEADER_SIZE;
      g_total_free -= size + HEADER_SIZE;

      // return pointer to new block made from the remainder of the block
      // we're keeping
      Block *block = (Block*)((uintptr_t)free_list_iterator +
                              HEADER_SIZE + free_list_iterator->size);
      // set the size parameter for the new block
      block->size = size;

      void *vp = (void *)((uintptr_t)block + HEADER_SIZE);
      check_heap();
      return vp;

    } else {
      // want to return the entire first block (don't need to split)
      void *vp = (void *)((uintptr_t)free_list + HEADER_SIZE);
      g_total_free -= free_list_iterator->size + HEADER_SIZE;
      free_list = free_list->next;
      g_free_blocks--;
      check_heap();
      return vp;
    }
  }

  // now treat the rest of the list
  while (free_list_iterator->next != NULL) {
    if (free_list_iterator->next->size >= size) {
      // if we need to split
      if (free_list_iterator->next->size - MIN_BLOCK_SIZE >=
          HEADER_SIZE + size) {
        // decriment the size of the block that is too large by amount requested
        free_list_iterator->next->size -= size + HEADER_SIZE;
        g_total_free -= size + HEADER_SIZE;

        // return pointer to new block made from the remainder of the
        // block we're keeping
        Block *block = (Block*)((uintptr_t)free_list_iterator->next +
                                HEADER_SIZE +
                                free_list_iterator->next->size);
        // set the size parameter for the new block
        block->size = size;

        void *vp = (void *)((uintptr_t)block + HEADER_SIZE);
        check_heap();
        return vp;

      } else {  // we don't need to split
        void *vp = (void *)((uintptr_t)free_list_iterator->next +
                            HEADER_SIZE);
        g_free_blocks--;
        g_total_free -= free_list_iterator->next->size + HEADER_SIZE;
        free_list_iterator->next = free_list_iterator->next->next;
        check_heap();
        return vp;
      }
    }
    free_list_iterator = free_list_iterator->next;
  }

  // if we've reached this point then no block on the list was large enough
  // to satisfy the users request, so we must allocate a new block

  // allocate 4096 + size (+ header sizes) bytes to some place in the free
  // list, return pointer to block with input size to user, keeping a block
  // with size 4096 on the free list in the correct place to maintain
  // increasing memory addresses
  Block *block = init_block(STANDARD_BLOCK_SIZE + size +
                            2 * HEADER_SIZE);
  block->size = STANDARD_BLOCK_SIZE;
  // create new block at the address where we want to split
  Block *split_block = (Block*)((uintptr_t)block + HEADER_SIZE +
                                 STANDARD_BLOCK_SIZE);
  // set the size parameter for the new block
  split_block->size = size;
  free_list_iterator = free_list;
  // make sure the new block staying on the free list maintains increasing
  // memory addresses
  if ((uintptr_t)block < (uintptr_t)free_list) {
    block->next = free_list;
    free_list = block;
    free_list_iterator = free_list;
  } else {
    while (free_list_iterator->next != NULL) {
      if ((uintptr_t)block < (uintptr_t)free_list_iterator->next) {
        block->next = free_list_iterator->next;
        free_list_iterator->next = block;
        break;
      }
      free_list_iterator = free_list_iterator->next;
    }
    // block goes at the very end
    if (free_list_iterator->next == NULL) {
      free_list_iterator->next = block;
      block->next = NULL;
    }
  }
  // subtract from the total free memory the total size of the block
  // we are returning
  g_total_free -= size + HEADER_SIZE;

  // return void pointer to the location of the end of the header of
  // the new block
  void *vp = (void *)((uintptr_t)split_block + HEADER_SIZE);
  check_heap();
  return vp;
}

