// Implementation of the function freemem
// Authors: Jason Ho, Thomas Johnson

#include <inttypes.h>
#include "mem_implEC.h"
#include "mem.h"

void check_combine(Block* first, Block* second);

// Takes a pointer p. If p is NULL, then call to freemem has no
// effect and returns immediately. Otherwise returns the block of
// storage at location p to the pool of available free storage.
void freemem(void* p) {
  if (p == NULL) {
    return;
  }

  check_heap();

  // get a Block* pointing to the block header
  Block *block = (Block*)((uintptr_t)p - HEADER_SIZE);

  block->free_flag = 1;

  g_free_blocks++;
  g_total_free += block->size + HEADER_SIZE;


  // combine if necessary
  
  // only check following if block is not the end
  if (block->next != NULL) {
    // combine with following block if necessary
    if (block->next->free_flag == 1) {
      check_combine(block, block->next);
    }
  }

  // only check prev if block is not the head
  if (block->prev != NULL) {
    // combine with previous is necessary
    if (block->prev->free_flag == 1) {
      check_combine(block->prev, block);
    }
  }

  check_heap();

  return;
}

// Helper function for freemem().
// Input parameters are free list blocks.
// Check to see if the blocks are touching, i.e the memory address
// at the end of the first block is the same as the memory address
// at the beginning of the second block. If the blocks are touching,
// combine the blocks by adding the memory space from the second block
// into the memory for the first block.
void check_combine(Block *first, Block *second) {
  uintptr_t end_of_first = (uintptr_t)first;
  end_of_first += HEADER_SIZE;
  end_of_first += first->size;
  if (end_of_first == (uintptr_t)second) {
    first->size += HEADER_SIZE;
    first->size += second->size;
    first->next = second->next;
    if (first->next != NULL) {
      first->next->prev = first;
    }
    g_free_blocks--;
  }
}
