# CSE 374 Homework 5, Memory Management

## Authors: Jason Ho and Thomas Johnson

### Jason's contributions

- freemem.c
- bench.c
- print_heap.c
- helped a lot in the debugging of getmem.c
- general headers, makefile, debugging and testing

### Thomas's contributions

- getmem.c
- get_mem_stats.c, check_heap.c
- general readme, headers, makefile, debugging and testing
- code review

#### Brief description of our heap data structure

To model the heap's dynamic memory allocation, we used a _free_ _list_ data strucure. The free list is a linked list where each node or block in the linked list models a chunk of memory. The data field in each block keeps track of the size in bytes that the block can allocate to the user. The free list blocks obtain this allocated memory with calls to the underlying operating system using malloc. The blocks on the free list have varying sizes and are ordered by memory address so that the user can be given the right amount of memory by iterating through the free list until a suitable block is found, or else a new block is added to the list.

#### Summary of any additional features or improvements in the memory manager or benchmark code

None so far.

#### Extra Credit

There are some files from an attempt at the extra credit, they can be built with "make benchEC" and run with the program benchEC, but there are no guarentees it will work properly.

#### Results from bench program

##### Some results for with default bench arguments:

iteration 1 0%
time elapsed 0.000040 seconds
total_size 0 :: total_free 0 :: total_used 0 :: free_blocks 0 :: average block size 0

iteration 1000 10%
time elapsed 0.000341 seconds
total_size 123632 :: total_free 73680 :: total_used 49952 :: free_blocks 18 :: average block size 4093

iteration 2000 20%
time elapsed 0.000692 seconds
total_size 147152 :: total_free 123712 :: total_used 23440 :: free_blocks 15 :: average block size 8247

iteration 3000 30%
time elapsed 0.000952 seconds
total_size 147152 :: total_free 124224 :: total_used 22928 :: free_blocks 12 :: average block size 10352

iteration 4000 40%
time elapsed 0.001228 seconds
total_size 147152 :: total_free 146944 :: total_used 208 :: free_blocks 7 :: average block size 20992

iteration 5000 50%
time elapsed 0.001488 seconds
total_size 147152 :: total_free 131136 :: total_used 16016 :: free_blocks 17 :: average block size 7713

##### Results for a large number of iterations:

teration 1 0%
time elapsed 0.000045 seconds
total_size 9632 :: total_free 4112 :: total_used 5520 :: free_blocks 1 :: average block size 4112

iteration 1000000 10%
time elapsed 2.494521 seconds
total_size 2077728 :: total_free 1378704 :: total_used 699024 :: free_blocks 167 :: average block size 8255

iteration 2000000 20%
time elapsed 5.972981 seconds
total_size 3407296 :: total_free 1049104 :: total_used 2358192 :: free_blocks 326 :: average block size 3218

iteration 3000000 30%
time elapsed 13.614373 seconds
total_size 4765776 :: total_free 2200720 :: total_used 2565056 :: free_blocks 446 :: average block size 4934

iteration 4000000 40%
time elapsed 23.005869 seconds
total_size 4847840 :: total_free 2514384 :: total_used 2333456 :: free_blocks 390 :: average block size 6447

iteration 5000000 50%
time elapsed 36.048008 seconds
total_size 6560832 :: total_free 2602416 :: total_used 3958416 :: free_blocks 607 :: average block size 4287

#### Summary of consulted resources

- Good visual explanation of free list operations:
http://courses.cs.washington.edu/courses/cse374/14wi/lectures/hw6_pictures.pdf

