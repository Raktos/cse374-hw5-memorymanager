// Implementation of the function freemem
// Authors: Jason Ho, Thomas Johnson

#include <inttypes.h>
#include "mem_impl.h"
#include "mem.h"

void check_combine(Block* first, Block* second);

// Takes a pointer p. If p is NULL, then call to freemem has no
// effect and returns immediately. Otherwise returns the block of
// storage at location p to the pool of available free storage.
void freemem(void* p) {
  if (p == NULL) {
    return;
  }

  check_heap();

  // get a Block* pointing to the block header
  Block *block = (Block*)((uintptr_t)p - HEADER_SIZE);
  Block *free_list_iterator = free_list;

  // block goes at the very beginning
  if ((uintptr_t)block < (uintptr_t)free_list) {
    block->next = free_list;
    free_list = block;
    free_list_iterator = free_list;
  } else {
    while (free_list_iterator->next != NULL) {
      if ((uintptr_t)block < (uintptr_t)free_list_iterator->next) {
        block->next = free_list_iterator->next;
        free_list_iterator->next = block;
        break;
      }

      free_list_iterator = free_list_iterator->next;
    }

    // block goes at the very end
    if (free_list_iterator->next == NULL) {
      free_list_iterator->next = block;
      block->next = NULL;
    }
  }

  g_free_blocks++;
  g_total_free += block->size + HEADER_SIZE;

  // combine if necessary

  // only check following if block is not the end
  if (block->next != NULL) {
    // combine with following block if necessary
    check_combine(block, block->next);
  }

  // only check prev if block is past the head
  if ((uintptr_t)block > (uintptr_t)free_list) {
    // combine with previous is necessary
    check_combine(free_list_iterator, block);
  }

  check_heap();

  return;
}

// Helper function for freemem().
// Input parameters are free list blocks.
// Check to see if the blocks are touching, i.e the memory address
// at the end of the first block is the same as the memory address
// at the beginning of the second block. If the blocks are touching,
// combine the blocks by adding the memory space from the second block
// into the memory for the first block.
void check_combine(Block *first, Block *second) {
  uintptr_t end_of_first = (uintptr_t)first;
  end_of_first += HEADER_SIZE;
  end_of_first += first->size;
  if (end_of_first == (uintptr_t)second) {
    first->size += HEADER_SIZE;
    first->size += second->size;
    first->next = second->next;
    g_free_blocks--;
  }
}
