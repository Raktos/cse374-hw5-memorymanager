// Public interface for clients of the bench program
// Authors: Jason Ho, Thomas Johnson

#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include "mem.h"

int main(int argc, char** argv) {
  // kill the program if we have too many args
  if (argc > 7) {
    fprintf(stderr, "usage bench [ntrials [pctget [pctlarge [small_limit "
                    "[large_limit [random_seed ]]]]]]\n");
    exit(1);
  }

  // setup a log file
  FILE *f = fopen("bench.log", "w");

  // initialize deault values fo args
  int ntrials = 10000;
  int pctget = 50;
  int pctlarge = 10;
  int small_limit = 200;
  int large_limit = 20000;
  int random_seed = (int)time(NULL);

  // initialize stats
  uintptr_t total_size = 0;
  uintptr_t total_free = 0;
  uintptr_t free_blocks = 0;

  // process user args
  for (int i = 1; i < argc; i++) {
    switch (i) {
    case 1:
      ntrials = atoi(argv[i]);
      break;
    case 2:
      pctget = atoi(argv[i]);
      break;
    case 3:
      pctlarge = atoi(argv[i]);
      break;
    case 4:
      small_limit = atoi(argv[i]);
      break;
    case 5:
      large_limit = atoi(argv[i]);
      break;
    case 6:
      random_seed = atoi(argv[i]);
      break;
    }
  }

  // used to determine which iterations to check stats on
  int stat_check = ntrials / 10;
  srand(random_seed);

  // initialize array to store getmem's
  int size_of_array = 0;
  void **ptrs = malloc(sizeof(void*) * ntrials);

  // initialize timer
  clock_t timer = clock();

  // perform actual test
  for (int i = 0; i < ntrials; i++) {
    // decide if we're getting memory or freeing it
    int get_or_free = 0;
    if (rand() % 100 < pctget) {
      get_or_free = 1;
    }

    // determine if we're getting a little block or a large one
    int large_or_small = 0;
    if (rand() % 100 < pctlarge) {
      large_or_small = 1;
    }

    // set the min and max sizes based on large_or-small
    int min_size = 1;
    int max_size = small_limit;
    if (large_or_small) {
      max_size = large_limit;
      min_size = small_limit;
    }

    // perform getmem or freemem
    if (get_or_free) {
      ptrs[size_of_array] = getmem(rand() % (max_size - min_size) + min_size);
      size_of_array++;
    } else {
      // only bother to free if we have something to free
      if (size_of_array > 0) {
        int index = rand() % size_of_array;
        freemem(ptrs[index]);
        ptrs[index] = ptrs[size_of_array - 1];
        size_of_array--;
      }
    }

    // if we're on a stat check iteration check and print stats
    if ((i + 1) % stat_check == 0 || i == 0) {
      get_mem_stats(&total_size, &total_free, &free_blocks);

      uintptr_t avg;
      if (free_blocks == 0) {
        avg = 0.0;
      } else {
	avg = total_free / free_blocks;
      }
      
      fprintf(f, "iteration %i %i%%\n", i + 1, (i + 1) / stat_check * 10);
      printf("iteration %i %i%%\n", i + 1, (i + 1) / stat_check * 10);
      fprintf(f, "time elapsed %f seconds\n",
              ((float)(clock() - timer) / CLOCKS_PER_SEC));
      printf("time elapsed %f seconds\n",
             ((float)(clock() - timer) / CLOCKS_PER_SEC));
      fprintf(f, "total_size %lu :: total_free %lu :: total_used %lu "
              ":: free_blocks %lu :: average block size %lu\n",
              total_size, total_free, total_size - total_free,
              free_blocks, avg);
      printf("total_size %lu :: total_free %lu :: total_used %lu "
             ":: free_blocks %lu :: average block size %lu\n\n",
             total_size, total_free, total_size - total_free, free_blocks, avg);
      print_heap(f);
    }
  }

  fclose(f);
  return 0;
}
