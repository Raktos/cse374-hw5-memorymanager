// Public interface for clients of the getmem/freemem package
// Authors: Jason Ho, Thomas Johnson

#ifndef MEM_H
#define MEM_H

#include <inttypes.h>
#include <stdio.h>

// Input "size" is an unsigned integer and must be greater than
// 0. If size <= 0 then return value is NULL. Otherwise returns
// a pointer to a new block of storage with at least "size" bytes
// of memory.
void* getmem(uintptr_t size);


// Takes a pointer p. If p is NULL, then call to freemem has no
// effect and returns immediately. Otherwise returns the block of
// storage at location p to the pool of available free storage.
void freemem(void* p);


// Store statistics about the current state of the memory manager
// in the three integer variables whose addresses are given as
// arguments.
//
// total_size: total amount of storage in bytes acquired by the
//             memory manager so far to use in satisfying allocation
//             requests
// total_free: the total amount of storage in bytes that is currently
//             stored in the free list
// n_free_blocks: the total number of individual blocks currently
//                stored on the free list
void get_mem_stats(uintptr_t* total_size, uintptr_t* total_free,
                   uintptr_t* n_free_blocks);


// Print a formatted listing to file f showing the blocks on the free
// list.
void print_heap(FILE* f);


#endif

