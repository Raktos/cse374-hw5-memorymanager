# makefile
# Authors: Jason Ho, Thomas Johnson

SHELL := /bin/bash

bench : bench.o getmem.o freemem.o get_mem_stats.o print_heap.o check_heap.o
	gcc -Wall -std=c11 -o bench bench.o getmem.o freemem.o get_mem_stats.o print_heap.o check_heap.o

benchEC: bench.o getmemEC.o freememEC.o get_mem_statsEC.o print_heapEC.o check_heapEC.o
	gcc -Wall -std=c11 -o benchEC bench.o getmemEC.o freememEC.o get_mem_statsEC.o print_heapEC.o check_heapEC.o

bench.o : bench.c mem.h
	gcc -Wall -std=c11 -g -c bench.c mem.h

getmem.o : getmem.c mem.h mem_impl.h
	gcc -Wall -std=c11 -g -c getmem.c mem.h mem_impl.h

freemem.o : freemem.c mem.h mem_impl.h
	gcc -Wall -std=c11 -g -c freemem.c mem.h mem_impl.h

get_mem_stats.o : get_mem_stats.c mem.h mem_impl.h
	gcc -Wall -std=c11 -g -c get_mem_stats.c mem.h mem_impl.h

print_heap.o : print_heap.c mem.h mem_impl.h
	gcc -Wall -std=c11 -g -c print_heap.c mem.h mem_impl.h

check_heap.o : check_heap.c mem.h mem_impl.h
	gcc -Wall -std=c11 -g -c check_heap.c mem.h mem_impl.h

getmemEC.o: getmemEC.c mem.h mem_implEC.h
	gcc -Wall -std=c11 -g -c getmemEC.c mem.h mem_implEC.h

freememEC.o : freememEC.c mem.h mem_implEC.h
	gcc -Wall -std=c11 -g -c freememEC.c mem.h mem_implEC.h

get_mem_statsEC.o : get_mem_statsEC.c mem.h mem_implEC.h
	gcc -Wall -std=c11 -g -c get_mem_statsEC.c mem.h mem_implEC.h

print_heapEC.o : print_heapEC.c mem.h mem_implEC.h
	gcc -Wall -std=c11 -g -c print_heapEC.c mem.h mem_implEC.h

check_heapEC.o : check_heapEC.c mem.h mem_implEC.h
	gcc -Wall -std=c11 -g -c check_heapEC.c mem.h mem_implEC.h

.PHONY : clean dist test med-test long-test mass-test

clean :
	rm -f *~ *\# *.log hw5.tar *.gch *.o bench benchEC
dist :
	git log > git.log && \
	tar -cvf hw5.tar mem.h mem_impl*.h getmem*.c freemem*.c print_heap*.c get_mem_stats*.c \
	check_heap*.c bench.c makefile git.log README.md

test : bench
	./bench 10000 50 10 200 20000 $$RANDOM

med-test: bench
	./bench 100000 50 10 200 20000 $$RANDOM

long-test: bench
	./bench 1000000 50 10 200 20000 $$RANDOM

mass-test: bench
	./bench 10000000 50 10 200 20000 $$RANDOM

testEC : benchEC
	./benchEC 10000 50 10 200 20000 $$RANDOM

med-testEC: benchEC
	./benchEC 100000 50 10 200 20000 $$RANDOM

long-testEC: benchEC
	./benchEC 1000000 50 10 200 20000 $$RANDOM

mass-testEC: benchEC
	./benchEC 10000000 50 10 200 20000 $$RANDOM
