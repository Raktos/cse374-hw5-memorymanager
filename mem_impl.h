// internal "public" interface for implementations of the getmem/freemem package
// Authors: Jason Ho, Thomas Johnson

#ifndef MEM_IMPL_H
#define MEM_IMPL_H

#include <inttypes.h>
#include <stdlib.h>

// Constants defintions
#define STANDARD_BLOCK_SIZE 4096
#define MIN_BLOCK_SIZE 32
#define HEADER_SIZE 16

// Block struct
// holds header information for a block of memory
//
// size: the size of the block excluding the header
//       information contained here
// next: points to the next avaliable block of free
//       memory in the list
typedef struct block {
  uintptr_t size;
  struct block *next;
} Block;

// global variables declarations
Block *free_list;
uintptr_t g_total_size;
uintptr_t g_total_free;
uintptr_t g_free_blocks;

// functions shared between implementation files

// Check for possible problems with the free list data structure.
// Makes the following checks:
// - blocks are ordered with strictly increasing memory addresses
// - block sizes are positive numbers and not smaller than the
//   chosen minimum block size
// - blocks do not overlap
// - blocks are not touching
// If no errors are detected than function returns silently.
void check_heap();

#endif
