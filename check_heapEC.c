// Implementation of the function check_heap
// Authors: Jason Ho, Thomas Johnson

#include "mem_implEC.h"
#include "mem.h"
#include <assert.h>

// Check for possible problems with the free list data structure.
// Makes the following checks:
// - blocks are ordered with strictly increasing memory addresses
// - block sizes are positive numbers and not smaller than the
//   chosen minimum block size
// - blocks do not overlap
// - blocks are not touching
// If no errors are detected than function returns silently.
void check_heap() {
  Block* free_list_iterator = free_list;
  while (free_list_iterator != NULL) {
    assert(free_list_iterator->size >= MIN_BLOCK_SIZE);

    if (free_list_iterator->next != NULL) {
      assert((uintptr_t)free_list_iterator < (uintptr_t)free_list_iterator->next);

      if (free_list_iterator->free_flag == 1 && free_list_iterator->next->free_flag == 1) {
        assert((uintptr_t)free_list_iterator + free_list_iterator->size + HEADER_SIZE != (uintptr_t)free_list_iterator->next);
        assert((uintptr_t)free_list_iterator + free_list_iterator->size + HEADER_SIZE < (uintptr_t)free_list_iterator->next);
      }
    }
    free_list_iterator = free_list_iterator->next;
  }
}
